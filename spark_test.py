from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming import DStream

"""Function i found on the internet"""
def updateFunc(new_values, last_sum):
    return sum(new_values) + (last_sum or 0)

sc = SparkContext("local[4]", appName="tw_test")

ssc = StreamingContext(sc, 1)
ssc.checkpoint("tweets")
batch = ssc.socketTextStream("127.0.0.1", 9999)
tokens = batch.filter(lambda x: x.strip('\n').split())
# To test sparks demo -- does not work very well on my machine...
wordcounts = tokens.map(lambda x: (x, 1)).reduceByKey(lambda x,y:x+y).map(lambda x:(x[1],x[0]))
wordcounts.pprint()
ssc.start()
ssc.awaitTerminationOrTimeout(30)
