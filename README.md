# README #

RUN gather_tweets.py first
In a new shell then submit spark_test through pyspark
At the waiting screen in gather_tweets, type any key once spark has started to start hitting twitters api

Code should just try to keep a freq count of words - spark on my machine is very buggy through python. Probably should have used scala.

The goal of this code was to have a python script that sent spark tweets over a socket. The python interface to spark does not have an inbuilt solution for handling the twitter api like scala does - which proved to be a bit trickier than I expected (time out issues, etc.). There must have been a better way to keep the connection alive over the socket.